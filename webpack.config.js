const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, './out/'),
        publicPath: '/out/',
        filename: '[name].js'
    },
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({filename: '[name].g.css'}),
    ].concat(
        process.env.OPT_CSS
        ? [new OptimizeCssAssetsPlugin({assetNameRegExp: /\.g\.css$/g})]
        : []
    )
}
