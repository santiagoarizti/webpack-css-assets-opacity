Percentage errors in Optimize Css Assets Webpack Plugin
=======================================================

Sample css file contains definitions in percentages.

When running webpack with 

    OPT_CSS=1 npx webpack

Contents of `out/main.g.css` are

    .angie{width:50%;font-size:70%;opacity:1%}

When running webpack with 

    npx webpack

Contents of `out/main.g.css` are

    .angie {
        width: 50%;
        font-size: 70%;
        opacity: 30%;
    }

Point is, opacity is being translated incorrectly.

MDN says, under **syntax** in the **css - opacity** page: _...A number in the range 0.0 to 1.0, inclusive, or a percentage in the range 0% to 100%, inclusive..._

